#!/usr/bin/env bash

set -euo pipefail

script_dir=$(dirname $(readlink -f $0))
pushd $script_dir

mkdir -p build
pushd build

rm -rf msvc msvc-wine
git clone https://github.com/mstorsjo/msvc-wine
pushd msvc-wine
./vsdownload.py --accept-license --dest ../msvc
./install.sh ../msvc

echo "msvc binaries are now available in $script_dir/build/msvc/bin/"
