#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" 1>&2
    exit 1
}

[ $# -ge 3 ] || die "usage: wine_dir wine_prefix cmd [args...]"

wine_dir=$1; shift
wine_prefix=$1; shift

export WINEPREFIX=$(readlink -f $wine_prefix)
export PATH=$wine_dir:$PATH
"$@"
