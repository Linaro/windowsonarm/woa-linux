#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" 1>&2
    exit 1
}

[ $# -eq 2 ] || die "usage: src_dir wine_version"

src_dir=$1; shift
wine_version=$1; shift
script_dir=$(dirname $(readlink -f $0))
patch_dir=$script_dir/patches

checkout()
{
    if [ -d $src_dir ]; then
        return
    fi

    # detect if version is 8.0, and replace by wine-8.0
    if [[ $wine_version =~ [0-9] ]]; then
        wine_version=wine-$wine_version
    fi

    echo "clone wine at version $wine_version"
    git clone https://github.com/wine-mirror/wine \
        --depth 1 --single-branch --branch $wine_version $src_dir

    pushd $src_dir

    # patch specific versions
    if [ $wine_version == wine-8.10 ]; then
        # https://bugs.winehq.org/show_bug.cgi?id=55051
        for patch in $patch_dir/wine-8.10/*; do
            echo "Apply $patch"
            patch -p1 < $patch
        done
    fi

    echo ----------------------------------------
    git --no-pager diff
    echo ----------------------------------------

    popd
}

compiler_config()
{
    cat << EOF
$(sha1sum $(which clang))
$(clang --version)
EOF
}

build()
{
    pushd $src_dir >&/dev/null

    touch .compiler_config
    local new_compiler=0
    diff .compiler_config <(compiler_config) || new_compiler=1

    if [ ! -f .configured ] || [ $new_compiler -eq 1 ]; then
        rm -f .configured
        git clean -ffdx
        compiler_config > .compiler_config

        local cc="ccache clang"
        $cc --version
        ./configure CC="$cc" x86_64_CC="$cc" aarch64_CC="$cc" \
            --enable-win64 --disable-tests \
            --without-unwind \
            --prefix=/usr
        touch .configured
    fi
    make -j$(nproc)
    popd >&/dev/null
}

checkout
build
