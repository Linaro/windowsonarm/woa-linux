#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" 1>&2
    exit 1
}

[ $# -ge 2 ] || die "usage: arch cmd [args...]
arch: x64 or arm64"

arch=$1; shift
script_dir=$(dirname $(readlink -f $0))

case $arch in
    x64|arm64) ;;
    *) die "unknown architecture $arch" ;;
esac

DEFAULT_WINE_VERSION=8.0
WINE_VERSION=${WINE_VERSION:-$DEFAULT_WINE_VERSION}

wine_dir=$script_dir/build/wine-${WINE_VERSION}-$arch
mkdir -p $wine_dir
wine_src=$wine_dir/src
wine_prefix=$wine_dir/prefix
# always delete prefix
rm -rf $wine_prefix

$script_dir/containers/run_arch.sh $arch $script_dir/wine/build.sh $wine_src $WINE_VERSION
$script_dir/containers/run_arch.sh $arch $script_dir/wine/run.sh $wine_src $wine_prefix "$@"
