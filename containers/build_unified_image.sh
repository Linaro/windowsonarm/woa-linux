#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" >&2
    exit 1
}

script_dir=$(readlink -f $(dirname $0))
build_dir=$script_dir/../build/
wine_dir=$script_dir/../wine/
mkdir -p $build_dir
dockerfile_base=$script_dir/build_wine.Dockerfile
dockerfile_unified=$script_dir/unified.Dockerfile

[ $# -eq 2 ] || die "usage: wine_version image_name"
wine_version=$1; shift
image_name=$1; shift

echo "build arm64 base image"
podman build -t woa-linux-base-arm64 --build-arg arch=arm64 - < $dockerfile_base

echo "build unified image"
podman build -t $image_name --build-arg WINE_VERSION=$wine_version $wine_dir -f $dockerfile_unified
