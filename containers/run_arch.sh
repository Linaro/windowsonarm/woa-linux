#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -ge 2 ] || die "usage: arch cmd [args...]
arch: x64 | arm64"

arch=$1; shift

script_dir=$(dirname $(readlink -f $0))
image=woa-linux-$arch
dockerfile=$script_dir/build_wine.Dockerfile

# build silently container
build_args="--build-arg arch=$arch -t $image"
ret=0
timeout 1 podman build $build_args - < $dockerfile >& /dev/null || ret=$?
if [ $ret -ne 0 ]; then
    podman build $build_args - < $dockerfile
fi

# run container
podman run \
    -it \
    --rm=true \
    -w "$(pwd)" \
    -e USER=$USER \
    -e HOME=$HOME \
    -v $HOME:$HOME \
    -v "$(pwd):$(pwd)" \
    $image "$@"
