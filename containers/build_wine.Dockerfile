ARG arch=arch_build_arg_must_be_set

FROM --platform=linux/$arch docker.io/debian:bookworm

ENV CLANG_VERSION=16

RUN apt update && apt install -y wget ca-certificates gpg
RUN echo "deb http://apt.llvm.org/bookworm/ llvm-toolchain-bookworm-${CLANG_VERSION} main" >> /etc/apt/sources.list
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# needs make, clang, dlltool (from llvm) and lld
RUN apt update && apt install -y clang-${CLANG_VERSION} llvm-${CLANG_VERSION} lld-${CLANG_VERSION}
RUN apt update && apt install -y build-essential
RUN apt update && apt install -y wget
RUN apt update && apt install -y git
RUN apt update && apt install -y ccache
RUN apt update && apt install -y gdb htop
RUN apt update && apt install -y strace sudo
RUN apt update && apt install -y locales-all

ENV LC_ALL=en_US.UTF-8

# link latest clang as clang
RUN ln -sf /usr/bin/clang-${CLANG_VERSION} /usr/bin/clang

# adds deps for msvc-wine
RUN apt update && apt install -y python3 msitools python3-simplejson python3-six ca-certificates winbind

# wine dependencies: https://wiki.winehq.org/Building_Wine
RUN apt update && apt install -y flex bison \
    libkrb5-dev libgnutls28-dev \
    libfreetype-dev \
    libx11-dev libxcomposite-dev libxcursor-dev libxfixes-dev libxi-dev \
    libxrandr-dev libxrender-dev libxext-dev
# fix freetype2 path
RUN mv /usr/include/freetype2/* /usr/include/
