FROM --platform=linux/arm64 woa-linux-base-arm64 AS rootfs-arm64

ARG WINE_VERSION=WINE_VERSION_must_be_set

# build wine-arm64 and install it
COPY / /tmp/wine/
RUN cd /tmp &&\
    CCACHE_DISABLE=1 ./wine/build.sh /tmp/wine-${WINE_VERSION} ${WINE_VERSION} && \
    cd /tmp/wine-${WINE_VERSION} && \
    make -j$(nproc) install && \
    rm -rf /tmp/*

# reduce image size
RUN apt purge -y llvm-* clang-* libclang-* libLLVM* && \
    apt autoremove -y && apt clean

################################################################################

FROM --platform=linux/amd64 docker.io/debian:bookworm AS image

ENV WINE_ARM64_ROOT=/opt/wine-arm64

# get rootfs from previous stage
COPY --from=rootfs-arm64 / $WINE_ARM64_ROOT/

# we use qemu for emulation
# get prebuilt version, so we can update it easily later
# we use the list of debian packages, sorted with last modified package first.
RUN apt update && apt install -y wget lynx binutils
RUN cd /tmp && \
    mkdir qemu && cd qemu && \
    QEMU_VERSION=9.2 &&\
    wget -q $(lynx -listonly -dump -nonumbers 'https://ftp.debian.org/debian/pool/main/q/qemu/?C=M;O=D' | \
    grep qemu-user_${QEMU_VERSION}.*amd64.deb | head -n 1) && \
    ar x *.deb && \
    tar xf data.tar.xz && \
    mv usr/bin/qemu-aarch64 /usr/bin && \
    rm -rf /tmp/*

# unsymlink aarch64 loader
RUN mv $(readlink -f $WINE_ARM64_ROOT/lib/ld-linux-aarch64.so.1) $WINE_ARM64_ROOT/lib/ld-linux-aarch64.so.1
# make aarch64 loader accessible from /lib too
# This is needed to run wine without using qemu (see WINE_NO_EMULATOR)
RUN cp $WINE_ARM64_ROOT/lib/ld-linux-aarch64.so.1 /lib/

# Turn symlinks to /lib/aarch64-linux-gnu into relative ones
RUN cd $WINE_ARM64_ROOT/usr/lib/aarch64-linux-gnu &&\
    for symlink in $(find -type l -maxdepth 1 | sort); do \
        dest=$(readlink $symlink | sed -e 's#^/.*aarch64-linux-gnu/##') && \
        ln -sfv $dest $symlink || exit 1; \
    done

# Create wrappers for wine binaries.
# Need to call them using qemu to not rely on binfmt.
# This is very slow on linux-arm64, as qemu-x64 and qemu-aarch64 stack.
# So, allow user to bypass this by setting WINE_NO_EMULATOR=1
#
# wine-preloader was moved from /usr/bin to /usr/lib/wine/aarch64-unix/
# with wine-10.2. We move it (optionally) in previous location to fix that.
RUN cd $WINE_ARM64_ROOT/usr/bin &&\
    (mv ../lib/wine/aarch64-unix/wine-preloader . || true) &&\
    ln -s $(pwd)/wine-preloader $WINE_ARM64_ROOT/usr/lib/wine/aarch64-unix/wine-preloader && \
    for bin in wine wineserver wine-preloader; do\
        mv $bin $bin.bin &&\
        echo '#!/usr/bin/env bash' > s &&\
        echo 'export LD_LIBRARY_PATH='$WINE_ARM64_ROOT'/usr/lib/aarch64-linux-gnu' >> s &&\
        echo 'emulator="qemu-aarch64"' >> s &&\
        echo '[ "$WINE_NO_EMULATOR" == "1" ] && emulator=' >> s &&\
        echo '$emulator '$WINE_ARM64_ROOT'/usr/bin/'$bin'.bin ''"$@"' >> s &&\
        chmod +x s && mv s $bin || exit 1; \
    done

# create wine-arm64 wrapper
# silence winedebug and set default prefix
RUN echo '#!/usr/bin/env bash' > w && \
    echo "wine_root=$WINE_ARM64_ROOT" >> w && \
    echo 'export WINEPREFIX=${WINEPREFIX:-$wine_root/wine-prefix}' >> w && \
    echo 'export WINEDEBUG=${WINEDEBUG:--all}' >> w && \
    echo '$wine_root/usr/bin/wine "$@"' >> w && \
    chmod +x w && mv w /usr/bin/wine-arm64

RUN apt update && apt install -y procps
# create wineprefix, and wait for wineserver end, to avoid corrupting prefix
# init prefix with WINE_NO_EMULATOR to go faster
RUN WINE_NO_EMULATOR=1 wine-arm64 cmd /c 'echo init wine prefix' && \
    while pgrep wineserver > /dev/null; do\
        echo "wait for wineserver to finish";\
        sleep 1;\
    done

################################################################################

# add some tools in published image
RUN apt update && apt install -y \
    unzip \
    curl \
    coreutils \
    git \
    python3 \
    rsync \
    file \
    xz-utils \
    ncdu \
    locales-all \
    xvfb

################################################################################

# test python arm64 works with wine-arm64
RUN cd /tmp && \
    wget -q https://www.nuget.org/api/v2/package/pythonarm64/3.11.3 -O python.zip && \
    unzip -q python.zip &&\
    WINE_NO_EMULATOR=1 wine-arm64 tools/python.exe -c 'print("Hello World from python", file=open("result", "w"))' &&\
    echo "Hello World from python" > expected &&\
    diff --strip-trailing-cr expected result &&\
    rm -rf /tmp/*

################################################################################

# squash final image (do not use scratch to force correct architecture)
FROM --platform=linux/amd64 docker.io/debian:bookworm
COPY --from=image / /

ENV LC_ALL=en_US.UTF-8

CMD bash
